package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/ping", pingpong)
	http.ListenAndServe(":9999", nil)
}

func pingpong(w http.ResponseWriter, r *http.Request) {
	log.Println("ping come")
	fmt.Fprintf(w, "pong")
}
